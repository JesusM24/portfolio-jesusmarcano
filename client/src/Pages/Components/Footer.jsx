import React from "react";
import {Row , Col} from "reactstrap"
import gitlab from "../Assets/svg/gitlab.svg"
import linkedin from "../Assets/svg/linkedin.svg"
import whatsapp from "../Assets/svg/whatsapp.svg"

const Footer = () => {
    return (
        <section id="footer">
            <Row className="g-0">
                <Col className="d-flex justify-content-center">
                    <span >Jesus Marcano 2023 ©</span>
                </Col>
            </Row>

            <Row className="g-0">
                <Col>

                    <div className="socialMedia d-flex w-100 justify-content-center">

                        <a href="https://gitlab.com/JesusM24" target="blank">
                            <img className="gitlab" src={gitlab} alt="Gitlab" />
                        </a>

                        <a href="https://www.linkedin.com/in/jes%C3%BAs-marcano-117b7024a/" target="blank">
                            <img className="linkedin" src={linkedin} alt="Gitlab" />
                        </a>

                        <a href="https://wa.link/8v62u9" target="blank">
                            <img className="whatsapp" src={whatsapp} alt="whatsapp" />
                        </a>
                    </div>
                </Col>
            </Row>
        </section>
    )
}

export default Footer;