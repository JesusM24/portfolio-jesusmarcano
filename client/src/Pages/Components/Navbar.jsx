import React from "react";
import { Row, Col } from 'reactstrap';
import logo from '../Assets/logo/LogoJM.png';

const Navbar = () => {
    return(
        <section id="Navbar">
            <div className="navbarContainer">
                <Row className="g-0">
                    <Col lg="1">
                        <div className="logoContainer">
                            <img src={logo} alt="Logo" className="logo"/>
                        </div>
                    </Col>

                    <Col lg="5">
                        <div className="nameContainer">
                            <p>Jesús Marcano - Frontend Developer</p>
                        </div>
                    </Col>

                    <Col lg="6">
                        <nav className="navigation d-flex justify-content-end">
                            <ul className="navList d-flex">
                                <li><a className="active" href="#">Home</a></li>
                                <li><a href="#">Portfolio</a></li>
                                <li><a href="#">Skills</a></li>
                                <li><a href="#">Contacto</a></li>
                            </ul>
                        </nav>
                    </Col>
                </Row>
            </div>
        </section>
    )
}

export default Navbar;