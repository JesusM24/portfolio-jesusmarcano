import React from "react";
import { useEffect } from "react";
import { useState } from "react";
import { Col } from "reactstrap";
import { SendEmail } from "../../../API";
import InlineError from "./InlineError";
import { validateEmail, validateMessage, validateName } from "./validation";
import {Toaster , toast} from 'react-hot-toast';

const Form = () => {

    const [name,setName] = useState("");
    const [email,setEmail] = useState("");
    const [message,setMessage] = useState("");
    const [nameError,setNameError] = useState();
    const [emailError,setEmailError] = useState();
    const [messageError,setMessageError] = useState();
    const [buttonLoading, setButtonLoading] = useState(false);
    const [send, setSend] = useState();


    useEffect(() =>{
        // ********** VALIDATION **********
        validateName({name,setNameError});
        validateEmail({email,setEmailError});
        validateMessage({message,setMessageError});

    /************ */
    if (send) {
        toast.success(send.msg)
        setName("");
        setEmail("");
        setMessage("");
        setSend()
    }

    },[name, email, message, send]);


    const submitHandler = (e) => {
        e.preventDefault();
        setButtonLoading(true)

        if (!nameError & !emailError & !messageError) {
            SendEmail({
                name,
                email,
                message,
                setSend,}).then(() =>{
                setButtonLoading(false)
                })
        } else {
            setButtonLoading(false)
        }
    };

    return(
        <section id="formSection">

            <svg preserveAspectRatio="none" viewBox="0 0 100 102" height="75" width="100%" version="1.1" xmlns="http://www.w3.org/2000/svg" class="svgcolor-light">
                <path d="M0 0 L50 100 L100 0 Z" fill="#EFFCEF" stroke="#655C56"></path>
            </svg>

            <div className="d-flex justify-content-center">
                <h1 className="title">CONTACTO</h1>
            </div>

            <div className="d-flex justify-content-center">
                <h3 className="text-center py-2">Te interesa mi perfil? ¡contáctate conmigo!</h3>
            </div>

            <form onSubmit={submitHandler} className="form">

                <div>
                    <div className="formContainer">
                            <Col className="d-flex justify-content-center label">
                                {/* Name */}
                                <label>Nombre</label>
                            </Col>

                            <Col className="d-flex justify-content-center my-2">

                                <input 
                                    value={name}
                                    onChange={(e) => setName(e.target.value)}
                                    className="name"
                                    required
                                    type="text"
                                    placeholder="nombre..."
                                />
                                    {nameError && <InlineError error={nameError} /> }
                            </Col>

                            <Col className="d-flex justify-content-center label">
                                {/* e-mail */}
                                <label>Correo</label>
                            </Col>

                            <Col className="d-flex justify-content-center my-2">
                                <input 
                                    value={email}
                                    onChange={(e) => setEmail(e.target.value)}
                                    className="email"
                                    required
                                    type="text"
                                    placeholder="alguien@ejemplo.com"
                                />
                                    {emailError && <InlineError error={emailError} /> }
                            </Col>

                            <Col className="d-flex justify-content-center label">
                                {/* Mensaje */}
                                <label>Mensaje</label>
                            </Col>

                            <Col className="d-flex justify-content-center my-2">
                                <textarea 
                                    value={message}
                                    onChange={(e) => setMessage(e.target.value)}
                                    name="Mensaje" 
                                    placeholder="Escribe un mensaje...">
                                </textarea>
                                
                                {messageError && <InlineError error={messageError} /> }

                            </Col>

                            <Col className="d-flex justify-content-center" >
                                <button
                                    type="submit"
                                    className="buttonForm"
                                    disabled={buttonLoading && true}>
                                        {buttonLoading ? "Cargando..." : "Enviar"}
                                </button>
                            </Col>
                    </div>
                </div>
            </form>

            <Toaster
                position="top-right"
                outoClose={2000}/>
        </section>
    )
}

export default Form;