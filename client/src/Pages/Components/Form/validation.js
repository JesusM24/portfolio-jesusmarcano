const validateEmail = ({ email, setEmailError }) => {
    const emailRegular = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return email && !email.match(emailRegular)
      ? setEmailError('El correo ingresado no es válido')
      : setEmailError('');
  };
  
  const validateName = ({ name, setNameError }) => {
    return name && name.length < 5
      ? setNameError('El nombre es muy corto')
      : name && name.length > 50
      ? setNameError('El nombre es muy largo')
      : setNameError('');
  };
  
  const validateMessage = ({ message, setMessageError }) => {
    return message && message.length < 4
    ? setMessageError('El mensaje es muy corto')
    : message && message.length > 500
    ? setMessageError('El mensaje es muy largo')
    : setMessageError('');
  };
  
  export { validateEmail, validateName, validateMessage };