import React from "react";

function InlineError({ error }) {
    return (
        <div>
            <p className="textError">{error}</p>
        </div>
    );
}

export default InlineError;