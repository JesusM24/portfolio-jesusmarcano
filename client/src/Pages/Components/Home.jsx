import React from "react";
import { Row, Col } from 'reactstrap';
import gitlab from '../Assets/Social-Media/gitlab.png'
import Linkedin from '../Assets/Social-Media/linkedin.png'
import whatsapp from '../Assets/Social-Media/whatsapp.png'

const Home = () => {
    return(
        <section id="Home">
            <div className="homeContainer">
                <Row className="g-0">
                    <Col>
                        <p className="presentation">¡HOLA!, MI NOMBRE ES JESUS MARCANO</p>
                    </Col>
                </Row>

                <Row className="g-0">
                    <Col lg="12" className="d-flex w-100 justify-content-center">
                        <p className="text-center aboutText ">
                            Soy un programador Junior en React.Js, con uso de ReactSrap,<br />
                            Bootstrap, Saas, consumo de API REST y testing con Postman,<br />
                            soy capaz de crear sitios web dinámicos.
                        </p>
                    </Col>
                </Row>
                <Row className="g-0">
                    <Col>
                        <div className="socialMedia d-flex w-100 justify-content-center">
                            <a className="gitlab" href="https://gitlab.com/JesusM24" target="blank" >
                                <img className="gitlab" src={gitlab} alt="Gitlab" />
                            </a>

                            <a href="https://www.linkedin.com/in/jes%C3%BAs-marcano-117b7024a/" target="blank" >
                                <img className="linkedin" src={Linkedin} alt="Linkedin" />
                            </a>

                            <a href="https://wa.link/8v62u9" target="blank" >
                                <img src={whatsapp} alt="whatsapp" />
                            </a>
                        </div>
                    </Col>
                </Row>
            </div>
        </section>
    )
}

export default Home;