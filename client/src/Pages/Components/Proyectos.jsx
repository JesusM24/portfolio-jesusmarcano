import React from "react";
import { Row, Col } from 'reactstrap';
import rootStudio from '../Assets/Proyects/root-studio.PNG';
import pokedex from '../Assets/Proyects/pokedex.PNG';

const Proyectos = () => {
    return(
        <section id="Proyectos">
            <Row className="g-0">
                <Col className="d-flex justify-content-center">
                    <h1 className="proyectosTitle">PROYECTOS</h1>
                </Col>
            </Row>

            <Row className="g-0">
                <Col lg="6">
                    <div className="rootContainer">
                        <a className="d-flex justify-content-center" 
                        href="https://root-studio.netlify.app/"
                        target="blank">
                            <img className="imgProyect" src={rootStudio} alt="" />
                        </a>
                    </div>
                </Col>

                <Col lg="6">
                    <div className="d-flex justify-content-center">
                        <h1 className="proyectTitle">ROOT-STUDIO</h1>
                    </div>

                    <div>
                        <p className="textProyect">
                            Este proyecto fue programado con React.js, bootstrap, Reactstrap y Saas. Además, posee responsive design. 
                        </p>
                    </div>

                    <div className="d-flex justify-content-center py-5">
                        <a
                        target="blank" 
                        href="https://root-studio.netlify.app/">
                            <button className="buttonProbar">
                                Probar
                            </button>
                        </a>

                        <a
                        target="blank" 
                        href="https://gitlab.com/JesusM24/root-studio">
                            <button className="buttonCodigo">
                                Ir al código
                            </button>
                        </a>
                    </div>
                </Col>
            </Row>

            <br /><br />

            <Row className="g-0">
                <Col lg="6">
                    <div className="d-flex justify-content-center">
                        <h1 className="proyectTitle">POKEDEX</h1>
                    </div>

                    <div>
                        <p className="textProyect">
                        En este proyecto logré consumir la API de pokémon, ordenando de manera ascendente todos los pokemones existentes, usando HTML, 
                        CSS, Javascript, Fetch y Paginación.
                        </p>
                    </div>

                    <div className="d-flex justify-content-center py-2">
                        <a
                        target="blank" 
                        href="https://myfirstpokedex1.netlify.app/">
                            <button className="buttonProbar">
                                Probar
                            </button>
                        </a>

                        <a
                        target="blank" 
                        href="https://gitlab.com/JesusM24/api-pokemon">
                            <button className="buttonCodigo">
                                Ir al código
                            </button>
                        </a>
                    </div>
                </Col>

                <Col lg="6">
                    <div className="pokedexContainer">
                        <a 
                        className="d-flex justify-content-center" 
                        href="https://myfirstpokedex1.netlify.app/"
                        target="blank">
                            <img className="imgProyect" src={pokedex} alt="pokedex" />
                        </a>
                    </div>
                </Col>
            </Row>
        </section>
    )
}

export default Proyectos;