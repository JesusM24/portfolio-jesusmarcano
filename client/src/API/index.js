import axios from 'axios';

// *********** Send email
export const SendEmail = async ({
    name,
    email,
    message,
    setSend,
  }) => {
    try {
      const datas = { name, email, message };
      let res = await axios.post(`http://localhost:3001/send`, datas); /*link del server al subirlo a netlify con el /send*/
      if (res) {
        setSend(res.data);
      }
    } catch (error) {
      alert(error.response.data.message);
    }
  };