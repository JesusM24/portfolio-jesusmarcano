import express from "express";
import dotenv from "dotenv";
import cors from "cors";
import EmailSender from "./SendEmail.js";

dotenv.config();
const app = express();
app.use(express.json());
app.use(cors({ origin: `${process.env.CLIENT_URL}` }));
const port = process.env.PORT || 3001;

// ****** SEND API
app.post("/send", async (req, res) => {
    try {
        const { name,email,message} = req.body
        EmailSender({name,email,message})
        res.json({ msg: "tu mensaje fue enviado!"});
    } catch (error) {
        res.status(404).json({ msg: "Error X"});
    }
});

app.listen(port, () => {
    console.log(`http://localhost:${port}`);
});